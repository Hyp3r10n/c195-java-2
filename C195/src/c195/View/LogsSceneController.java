/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195.View;

import c195.Model.LoggerPro;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Stream;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

/**
 * FXML Controller class
 *
 * @author limpFish
 */
public class LogsSceneController implements Initializable {

    @FXML
    private TextArea logTextArea;
    @FXML
    private Button returnButton;
    @FXML
    private Button quitButton;
    Stream<String> stream;
    Utilities util = new Utilities();

    @FXML
    private void returnButtonPushed(ActionEvent event) throws IOException {
        util.switchScene(event, "MainScene.fxml");
    }

    @FXML
    private void quitButtonPushed(ActionEvent event) throws IOException {
        util.quitProgram();
    }

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        LoggerPro logger = new LoggerPro();
        stream = logger.readLogs();
        String[] array = stream.toArray(String[]::new);
        for (String s : array) {
            logTextArea.appendText(s + "\n");
        }
    }

}
