/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195.View;

import c195.Helpers.DatabaseUtils;
import c195.Model.User;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author limpFish
 */
public class AddAppointmentSceneController implements Initializable {

    @FXML
    private ComboBox customerComboBox;
    @FXML
    private ComboBox consultantComboBox;
    @FXML
    private ComboBox locationComboBox;
    @FXML
    private ComboBox contactComboBox;
    @FXML
    private ComboBox typeComboBox;
    @FXML
    private TextField titleTextField;
    @FXML
    private TextField descriptionTextField;
    @FXML
    private TextField meetingUrlTextField;
    @FXML
    private ComboBox startTimeComboBox;
    @FXML
    private ComboBox endTimeComboBox;
    @FXML
    private DatePicker appointmentDatePicker;
    @FXML
    private Button saveButton;
    @FXML
    private Button cancelButton;
    private ObservableList<String> startTimes = FXCollections.observableArrayList();
    private ObservableList<String> endTimes = FXCollections.observableArrayList();
    private DateTimeFormatter timeDTF = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT);
    Utilities util = new Utilities();

    @FXML
    private void saveButtonPushed(ActionEvent event) throws IOException, SQLException {
        // TO DO --> check for times that are within business hours
        // TO DO --> check to confirm that BOTH the customer and consultant are available for that time
        // 1. pull all values from the fields
        String customerName = customerComboBox.getValue().toString();
        String userName = consultantComboBox.getValue().toString();

        PreparedStatement userCustomerIds = DatabaseUtils.getDBConnection().prepareStatement(
                "select customerId, userId from customer, user where customerName = ? and userName = ?;");
        userCustomerIds.setString(1, customerName);
        userCustomerIds.setString(2, userName);

        ResultSet ids = userCustomerIds.executeQuery();
        ids.next();

        int customerId = ids.getInt("customerId");
        int userId = ids.getInt("userId");
        String localDate = appointmentDatePicker.getValue().toString();

        String startDateTime = localDate + " " + startTimeComboBox.getValue().toString();
        String endDateTime = localDate + " " + endTimeComboBox.getValue().toString();
        System.out.println("startDateTime: " + startDateTime);
        System.out.println("endDateTime: " + endDateTime);
        PreparedStatement appointmentInsertSQL = DatabaseUtils.getDBConnection().prepareStatement(
                "insert into appointment (customerId, userId, title, description, location, contact, type, url, start, end, createDate, createdBy, lastUpdate, lastUpdateBy) "
                + "values "
                + "(?, ?, ?, ?, ?, ?, ?, ?, STR_TO_DATE(?, '%Y-%m-%d %h:%i %p'), STR_TO_DATE(?, '%Y-%m-%d %h:%i %p'), now(), ?, now(), ?);");

        appointmentInsertSQL.setInt(1, customerId);
        appointmentInsertSQL.setInt(2, userId);
        appointmentInsertSQL.setString(3, titleTextField.getText());
        appointmentInsertSQL.setString(4, descriptionTextField.getText());
        appointmentInsertSQL.setString(5, locationComboBox.getValue().toString());
        appointmentInsertSQL.setString(6, contactComboBox.getValue().toString());
        appointmentInsertSQL.setString(7, typeComboBox.getValue().toString());
        appointmentInsertSQL.setString(8, meetingUrlTextField.getText());
        appointmentInsertSQL.setString(9, startDateTime);
        appointmentInsertSQL.setString(10, endDateTime);
        appointmentInsertSQL.setString(11, User.getUserName());
        appointmentInsertSQL.setString(12, User.getUserName());

        appointmentInsertSQL.executeUpdate();

        util.switchScene(event, "CalendarScene.fxml");
    }

    @FXML
    private void cancelButtonPushed(ActionEvent event) throws IOException {
        util.switchScene(event, "CalendarScene.fxml");
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            locationComboBox.getItems().addAll("New York", "Phoenix", "London");
            typeComboBox.getItems().addAll("Initial meet", "Consultation", "Closing meet");

            PreparedStatement consultantContactListSQL = DatabaseUtils.getDBConnection().prepareStatement("select userName from user;");
            ResultSet consultantContactList = consultantContactListSQL.executeQuery();

            while (consultantContactList.next()) {
                consultantComboBox.getItems().add(consultantContactList.getString("userName"));
                contactComboBox.getItems().add(consultantContactList.getString("userName"));
            }

            PreparedStatement customerListSQL = DatabaseUtils.getDBConnection().prepareStatement("select customerName from customer;");
            ResultSet customerList = customerListSQL.executeQuery();

            while (customerList.next()) {
                customerComboBox.getItems().add(customerList.getString("customerName"));
            }
            /**
             * Sets time based on assumption of Business Hours being 8am - 5pm
             * So does not allow time selection outside of Business Hours
             */
            LocalTime time = LocalTime.of(8, 0);
            do {
                startTimes.add(time.format(timeDTF));
                endTimes.add(time.format(timeDTF));
                time = time.plusMinutes(15);
            } while (!time.equals(LocalTime.of(17, 15)));
            startTimes.remove(startTimes.size() - 1);
            endTimes.remove(0);

            appointmentDatePicker.setValue(LocalDate.now());

            startTimeComboBox.setItems(startTimes);
            endTimeComboBox.setItems(endTimes);
            startTimeComboBox.getSelectionModel().select(LocalTime.of(8, 0).format(timeDTF));
            endTimeComboBox.getSelectionModel().select(LocalTime.of(8, 15).format(timeDTF));

        } catch (SQLException ex) {
            Logger.getLogger(AddAppointmentSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
