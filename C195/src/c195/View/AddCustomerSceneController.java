/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195.View;

import c195.Model.User;
import c195.Helpers.DatabaseUtils;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author limpFish
 */
public class AddCustomerSceneController implements Initializable {

    @FXML
    private TextField nameTextField;
    @FXML
    private TextField phoneTextField;
    @FXML
    private TextField addressTextField;
    @FXML
    private TextField address2TextField;
    @FXML
    private TextField postalCodeTextField;
    @FXML
    private Label countryLabel;
    @FXML
    private Button saveButton;
    @FXML
    private Button cancelButton;
    @FXML
    private ComboBox cityComboBox;
    public Utilities util = new Utilities();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cityComboBox.getItems().addAll("New York", "Phoenix", "London");

        cityComboBox.setOnAction((e) -> {
            String country = cityComboBox.getSelectionModel().getSelectedItem().toString();
            switch (country) {
                case "New York":
                    country = "United States";
                    break;
                case "Phoenix":
                    country = "United States";
                    break;
                case "London":
                    country = "United Kingdom";
                    break;
                default:
                    country = "Unknown";
                    break;
            }

            countryLabel.setText(country);

        });
    }

    @FXML
    private void saveButtonPushed(ActionEvent event) throws SQLException, IOException {

        String cityResult = cityComboBox.getSelectionModel().getSelectedItem().toString();

        PreparedStatement citySQL = DatabaseUtils.getDBConnection().prepareStatement("SELECT city.cityId, city.city, country.countryId, country.country FROM city inner join country on city.countryId = country.countryId where city = ?;");
        citySQL.setString(1, cityResult);
        ResultSet result = citySQL.executeQuery();

        result.next();
        String cityId = result.getString("cityId");

        PreparedStatement addressInsert = DatabaseUtils.getDBConnection().prepareStatement(
                "INSERT INTO address"
                + "(address, address2, cityId, postalCode, phone, createDate, createdBy, lastUpdate, lastUpdateBy) "
                + "VALUES (?, ?, ?, ?, ?, now(), ?, now(), ?);");
        addressInsert.setString(1, addressTextField.getText());
        addressInsert.setString(2, address2TextField.getText());
        addressInsert.setString(3, cityId);
        addressInsert.setString(4, postalCodeTextField.getText());
        addressInsert.setString(5, phoneTextField.getText());
        addressInsert.setString(6, User.getUserName());
        addressInsert.setString(7, User.getUserName());
        addressInsert.executeUpdate();

        PreparedStatement addressIdSQL = DatabaseUtils.getDBConnection().prepareStatement(
                "select addressId from address where address = ? and postalCode = ? and phone = ?;");
        addressIdSQL.setString(1, addressTextField.getText());
        addressIdSQL.setString(2, postalCodeTextField.getText());
        addressIdSQL.setString(3, phoneTextField.getText());
        ResultSet idResult = addressIdSQL.executeQuery();
        idResult.next();
        int addressId = idResult.getInt("addressId");

        PreparedStatement customerInsert = DatabaseUtils.getDBConnection().prepareStatement(
                "insert into customer (customerName, addressId, active, createDate, createdBy, lastUpdate, lastUpdateBy) values (?, ?, 1, now(), ?, now(), ?);");
        customerInsert.setString(1, nameTextField.getText());
        customerInsert.setInt(2, addressId);
        customerInsert.setString(3, User.getUserName());
        customerInsert.setString(4, User.getUserName());
        customerInsert.executeUpdate();

        util.switchScene(event, "CustomersScene.fxml");

    }

    @FXML
    private void cancelButtonPushed(ActionEvent event) throws IOException {
        util.switchScene(event, "CustomersScene.fxml");
    }

}
