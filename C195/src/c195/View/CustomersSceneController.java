/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195.View;

import c195.Model.Customer;
import c195.Helpers.DatabaseUtils;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class CustomersSceneController implements Initializable {

    @FXML
    private TableView<Customer> customerTableView;
    @FXML
    private TableColumn<Customer, String> customerTableNameTableColumn;
    @FXML
    private TableColumn<Customer, String> customerTableAddressTableColumn;
    @FXML
    private TableColumn<Customer, String> customerTablePhoneNumberTableColumn;
    @FXML
    private Button addCustomerButton;
    @FXML
    private Button modifyCustomerButton;
    @FXML
    private Button deleteCustomerButton;
    @FXML
    private Button returnButton;
    @FXML
    private Button quitButton;
    Utilities util = new Utilities();

    @FXML
    private void addCustomerButtonPushed(ActionEvent event) throws IOException {
        util.switchScene(event, "AddCustomerScene.fxml");
    }

    @FXML
    private void modifyCustomerButtonPushed(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("ModifyCustomerScene.fxml"));
        Parent modifyCustomerScreenParent = loader.load();
        ModifyCustomerSceneController modifyCustomerSceneController = loader.getController();

        // Send the data into ModifyCustomerSceneController
        modifyCustomerSceneController.initData(customerTableView.getSelectionModel().getSelectedItem());

        Scene modifyCustomerScreenScene = new Scene(modifyCustomerScreenParent);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(modifyCustomerScreenScene);
        window.show();

    }

    @FXML
    private void deleteCustomerButtonPushed(ActionEvent event) throws SQLException {
        try {
            String customerId;
            String addressId;
            String selectedCustomer = Integer.toString(customerTableView.getSelectionModel().getSelectedItem().getCustomerId());

            PreparedStatement customerAddressIds = DatabaseUtils.getDBConnection().prepareStatement("Select customer.customerId, customer.addressId from customer where customerId = ?");
            customerAddressIds.setString(1, selectedCustomer);
            ResultSet resultIds = customerAddressIds.executeQuery();

            resultIds.next();

            customerId = resultIds.getString("customer.customerId");
            addressId = resultIds.getString("customer.addressId");

            PreparedStatement deleteCustomer = DatabaseUtils.getDBConnection().prepareStatement("Delete customer.* from customer where customerId = ?");
            deleteCustomer.setString(1, customerId);
            deleteCustomer.executeUpdate();
            PreparedStatement deleteAddress = DatabaseUtils.getDBConnection().prepareStatement("Delete address.* from address where address.addressId = ?");
            deleteAddress.setString(1, addressId);
            deleteAddress.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        updateCustomerTableView();
    }

    @FXML
    private void returnButtonPushed(ActionEvent event) throws IOException {
        util.switchScene(event, "MainScene.fxml");
    }

    @FXML
    private void quitButtonPushed(ActionEvent event) throws IOException {
        util.quitProgram();
    }

    public void updateCustomerTableView() throws SQLException {
        String customerId;
        String name;
        String address;
        String address2;
        String city;
        String country;
        String postalCode;
        String phone;

        ObservableList<Customer> customers = FXCollections.observableArrayList();
        try {
            PreparedStatement sql = DatabaseUtils.getDBConnection().prepareStatement("Select customer.customerId, customer.customerName, address.address, address.address2, address.phone, address.postalCode, city.city, country.country "
                    + "from customer "
                    + "inner join address on customer.addressId = address.addressId "
                    + "inner join city on address.cityId = city.cityId "
                    + "inner join country on city.countryId = country.countryId;");
            ResultSet result = sql.executeQuery();

            while (result.next()) {
                customerId = result.getString("customer.customerId");
                name = result.getString("customer.customerName");
                address = result.getString("address.address");
                address2 = result.getString("address.address2");
                city = result.getString("city.city");
                country = result.getString("country.country");
                postalCode = result.getString("address.postalCode");
                phone = result.getString("address.phone");

                Customer customer = new Customer(Integer.parseInt(customerId),
                        name,
                        address,
                        address2,
                        city,
                        country,
                        postalCode,
                        phone);

                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        customerTableView.getItems().setAll(customers);
    }

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        customerTableNameTableColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("name"));
        customerTableAddressTableColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("totalAddress"));
        customerTablePhoneNumberTableColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("phone"));

        try {
            updateCustomerTableView();
        } catch (SQLException ex) {
            Logger.getLogger(CustomersSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
