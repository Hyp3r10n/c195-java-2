/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195.Model;

/**
 *
 * @author limpFish
 */
public class Customer {

    private int customerId;
    private String name;
    private String address;
    private String address2;
    private String city;
    private String country;
    private String postalCode;
    private String totalAddress;
    private String phone;

    public Customer(int customerId, String name, String address, String address2, String city, String country, String postalCode, String phone) {
        this.customerId = customerId;
        this.name = name;
        this.address = address;
        this.address2 = address2;
        this.city = city;
        this.country = country;
        this.postalCode = postalCode;
        this.totalAddress = this.address + " " + this.address2 + " " + this.city + " " + this.postalCode + " " + this.country;
        this.phone = phone;
    }

    public Customer(int customerId, String customerName) {
        this.customerId = customerId;
        this.name = customerName;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getCustomerId() {
        return this.customerId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setAddress(String address) {
        this.address = address;
        setTotalAddress();
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
        setTotalAddress();
    }

    public String getAddress2() {
        return this.address2;
    }

    public void setCity(String city) {
        this.city = city;
        setTotalAddress();
    }

    public String getCity() {
        return this.city;
    }

    public void setCountry(String country) {
        this.country = country;
        setTotalAddress();
    }

    public String getCountry() {
        return this.country;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
        setTotalAddress();
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setTotalAddress() {
        this.totalAddress = this.address + " " + this.address2 + " " + this.city + " " + this.postalCode + " " + this.country;
    }

    public String getTotalAddress() {
        return this.totalAddress;
    }

}
