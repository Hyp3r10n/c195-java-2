/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195.View;

import c195.Helpers.DatabaseUtils;
import c195.Model.Appointment;
import c195.Model.User;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author limpFish
 */
public class ReportsSceneController implements Initializable {

    @FXML
    private Tab sheduleTab;
    @FXML
    private TableView<Appointment> scheduleTableView;
    @FXML
    private TableColumn<Appointment, String> titleTableColumn;
    @FXML
    private TableColumn<Appointment, String> descriptionTableColumn;
    @FXML
    private TableColumn<Appointment, String> typeTableColumn;
    @FXML
    private TableColumn<Appointment, LocalDate> startTableColumn;
    @FXML
    private TableColumn<Appointment, LocalDate> endTableColumn;
    @FXML
    private TableColumn<Appointment, String> customerTableColumn;
    @FXML
    private Tab appointmentTypeByMonthTab;
    @FXML
    private TableView<Appointment> appointmentTypeByMonthTableView;
    @FXML
    private TableColumn<Appointment, String> monthTableColumn;
    @FXML
    private TableColumn<Appointment, String> appointmentTypeTableColumn;
    @FXML
    private TableColumn<Appointment, Integer> countOfTypesTableColumn;
    @FXML
    private Tab customerTotalAppointmentsTab;
    @FXML
    private TableView<Appointment> customerTotalAppointmentsTableView;
    @FXML
    private TableColumn<Appointment, String> customerApptTableColumn;
    @FXML
    private TableColumn<Appointment, String> appointmentApptTypeTableColumn;
    @FXML
    private TableColumn<Appointment, Integer> countOfApptTypesTableColumn;
    @FXML
    private Button returnButton;
    @FXML
    private Button quitButton;
    Utilities util = new Utilities();
    int userId = User.getUserId();

    @FXML
    private void returnButtonPushed(ActionEvent event) throws IOException {
        util.switchScene(event, "MainScene.fxml");
    }

    @FXML
    private void quitButtonPushed(ActionEvent event) throws IOException {
        util.quitProgram();
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) { // TO DO --> consider doing appointment types by location to keep all table views as appointment
        try {
            // TODO
            
            populateScheduleTableView();
            populateAppointmentTypeByMonthTableView();
            populateCustomerTotalAppointmentsTableView();
            
            // TO DO --> attach data to table columns: Shedule
            titleTableColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
            descriptionTableColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
            typeTableColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
            startTableColumn.setCellValueFactory(new PropertyValueFactory<>("start"));
            endTableColumn.setCellValueFactory(new PropertyValueFactory<>("end"));
            customerTableColumn.setCellValueFactory(new PropertyValueFactory<>("customer"));
            // TO DO --> attach data to table columns: Appt by month
            // TO DO --> attach data to table columns: Customer total appt
        } catch (SQLException ex) {
            Logger.getLogger(ReportsSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void populateScheduleTableView() throws SQLException {
        ObservableList<Appointment> sched = FXCollections.observableArrayList();

        PreparedStatement scheduleSelect = DatabaseUtils.getDBConnection().prepareStatement(
                "select cast(appointment.appointmentId as char) \"appointment.appointmentId\", "
                + "appointment.title, "
                + "appointment.description, "
                + "appointment.type, "
                + "cast(appointment.`start` as char) \"appointment.start\", "
                + "cast(appointment.`end` as char) \"appointment.end\", "
                + "customer.customerName "
                + "from appointment "
                + "inner join customer on customer.customerId = appointment.appointmentId "
                + "where appointment.userId = ?");
        scheduleSelect.setInt(1, userId);
        ResultSet schedule = scheduleSelect.executeQuery();

//        appointmentId;
//    private String customer;
//    private String type;
//    private String description;
//    private String start;
//    private String end;
//    private String user;
        while (schedule.next()) {
            sched.add(new Appointment(
                    schedule.getString("appointment.appointmentId"),
                    schedule.getString("customer.customerName"),
                    schedule.getString("appointment.type"),
                    schedule.getString("appointment.description"),
                    schedule.getString("appointment.`start`"),
                    schedule.getString("appointment.`end`"),
                    User.getUserName()));
        }

<<<<<<< HEAD
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
=======
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
>>>>>>> d5d3639f3b3f5f565ce1024b6639ba192c5d37ba
    }

    private void populateAppointmentTypeByMonthTableView() {
//        ObservableList<Appointment> apptTypeByMonth = FXCollections.observableArrayList();
//
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void populateCustomerTotalAppointmentsTableView() {
//        ObservableList<Appointment> custTotAppt = FXCollections.observableArrayList();
//
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
