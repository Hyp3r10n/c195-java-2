/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195.View;

import c195.View.AddAppointmentSceneController;
import c195.Model.User;
import c195.Helpers.DatabaseUtils;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author limpFish
 */
public class ModifyAppointmentSceneController implements Initializable {

    @FXML
    private ComboBox customerComboBox;
    @FXML
    private ComboBox consultantComboBox;
    @FXML
    private ComboBox locationComboBox;
    @FXML
    private ComboBox contactComboBox;
    @FXML
    private ComboBox typeComboBox;
    @FXML
    private TextField titleTextField;
    @FXML
    private TextField descriptionTextField;
    @FXML
    private TextField meetingUrlTextField;
    @FXML
    private ComboBox startTimeComboBox;
    @FXML
    private ComboBox endTimeComboBox;
    @FXML
    private DatePicker appointmentDatePicker;
    @FXML
    private Button saveButton;
    @FXML
    private Button cancelButton;
    private ObservableList<String> startTimes = FXCollections.observableArrayList();
    private ObservableList<String> endTimes = FXCollections.observableArrayList();
    private DateTimeFormatter timeDTF = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT);
    private int apptID;
    Utilities util = new Utilities();

    @FXML
    private void saveButtonPushed(ActionEvent event) throws IOException, SQLException {
        // TO DO --> check for times that are within business hours
        // TO DO --> check to confirm that BOTH the customer and consultant are available for that time
        String customerName = customerComboBox.getValue().toString();
        String userName = consultantComboBox.getValue().toString();

        PreparedStatement userCustomerIds = DatabaseUtils.getDBConnection().prepareStatement(
                "select customerId, userId from customer, user where customerName = ? and userName = ?;");
        userCustomerIds.setString(1, customerName);
        userCustomerIds.setString(2, userName);

        ResultSet ids = userCustomerIds.executeQuery();
        ids.next();

        int customerId = ids.getInt("customerId");
        int userId = ids.getInt("userId");
        String localDate = appointmentDatePicker.getValue().toString();

        String startDateTime = localDate + " " + startTimeComboBox.getValue().toString();
        String endDateTime = localDate + " " + endTimeComboBox.getValue().toString();
        PreparedStatement appointmentInsert = DatabaseUtils.getDBConnection().prepareStatement(
                "update appointment "
                + "set customerId = ?, "
                + "userId = ?, "
                + "title = ?, "
                + "description = ?, "
                + "location = ?, "
                + "contact = ?, "
                + "type = ?, "
                + "url = ?, "
                + "`start` = STR_TO_DATE(?, '%Y-%m-%d %h:%i %p'), "
                + "`end` = STR_TO_DATE(?, '%Y-%m-%d %h:%i %p'), "
                + "lastUpdate = now(), "
                + "lastUpdateBy = ?"
                + "where appointmentId = ?;");

        appointmentInsert.setInt(1, customerId);
        appointmentInsert.setInt(2, userId);
        appointmentInsert.setString(3, titleTextField.getText());
        appointmentInsert.setString(4, descriptionTextField.getText());
        appointmentInsert.setString(5, locationComboBox.getValue().toString());
        appointmentInsert.setString(6, contactComboBox.getValue().toString());
        appointmentInsert.setString(7, typeComboBox.getValue().toString());
        appointmentInsert.setString(8, meetingUrlTextField.getText());
        appointmentInsert.setString(9, startDateTime);
        appointmentInsert.setString(10, endDateTime);
        appointmentInsert.setString(11, User.getUserName());
        appointmentInsert.setInt(12, apptID);

        appointmentInsert.executeUpdate();

        util.switchScene(event, "CalendaryScene.fxml");
    }

    @FXML
    private void cancelButtonPushed(ActionEvent event) throws IOException {
        util.switchScene(event, "CalendarScene.fxml");
    }

    @FXML
    public void initData(int appointmentId) throws SQLException {
        PreparedStatement appointmentSelect = DatabaseUtils.getDBConnection().prepareStatement(
                "select appointment.title, appointment.description, appointment.url, appointment.location, appointment.type, "
                + "cast(appointment.`start` as date) \"appointment.date\", "
                + "case when left(time_format(`start`, '%h:%i %p'),1) = '0' then right(time_format(`start`, '%h:%i %p'),7) else time_format(`start`, '%h:%i %p') end \"appointment.startTime\", "
                + "case when left(time_format(`end`, '%h:%i %p'),1) = '0' then right(time_format(`end`, '%h:%i %p'),7) else time_format(`end`, '%h:%i %p') end \"appointment.endTime\", "
                + "appointment.contact, user.userName, customer.customerName "
                + "from appointment "
                + "inner join customer on customer.customerId = appointment.customerId "
                + "inner join user on user.userId = appointment.userId "
                + "where appointmentId = ?;");

        appointmentSelect.setInt(1, appointmentId);
        ResultSet appointment = appointmentSelect.executeQuery();
        appointment.next();
        titleTextField.setText(appointment.getString("appointment.title"));
        descriptionTextField.setText(appointment.getString("appointment.description"));
        meetingUrlTextField.setText(appointment.getString("appointment.url"));
        appointmentDatePicker.setValue(LocalDate.parse(appointment.getString("appointment.date")));
        customerComboBox.getSelectionModel().select(appointment.getString("customer.customerName"));
        consultantComboBox.getSelectionModel().select(appointment.getString("user.userName"));
        locationComboBox.getSelectionModel().select(appointment.getString("appointment.location"));
        contactComboBox.getSelectionModel().select(appointment.getString("appointment.contact"));
        typeComboBox.getSelectionModel().select(appointment.getString("appointment.type"));
        startTimeComboBox.getSelectionModel().select(appointment.getString("appointment.startTime"));
        endTimeComboBox.getSelectionModel().select(appointment.getString("appointment.endTime"));

        apptID = appointmentId;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // TODO --> initialize ALL combo box options to select
            // contact & consultant will use same code to populate their boxes
            locationComboBox.getItems().addAll("New York", "Phoenix", "London");
            typeComboBox.getItems().addAll("Initial meet", "Consultation", "Closing meet");

            PreparedStatement consultantContactListSQL = DatabaseUtils.getDBConnection().prepareStatement("select userName from user;");
            ResultSet consultantContactList = consultantContactListSQL.executeQuery();

            while (consultantContactList.next()) {
                consultantComboBox.getItems().add(consultantContactList.getString("userName"));
                contactComboBox.getItems().add(consultantContactList.getString("userName"));
            }

            PreparedStatement customerListSQL = DatabaseUtils.getDBConnection().prepareStatement("select customerName from customer;");
            ResultSet customerList = customerListSQL.executeQuery();

            while (customerList.next()) {
                customerComboBox.getItems().add(customerList.getString("customerName"));
            }
            /**
             * Sets time based on assumption of Business Hours being 8AM - 5PM
             * So does not allow time selection outside of Business Hours
             */
            LocalTime time = LocalTime.of(8, 0);
            do {
                startTimes.add(time.format(timeDTF));
                endTimes.add(time.format(timeDTF));
                time = time.plusMinutes(15);
            } while (!time.equals(LocalTime.of(17, 15)));
            startTimes.remove(startTimes.size() - 1);
            endTimes.remove(0);

            appointmentDatePicker.setValue(LocalDate.now());

            startTimeComboBox.setItems(startTimes);
            endTimeComboBox.setItems(endTimes);
            startTimeComboBox.getSelectionModel().select(LocalTime.of(8, 0).format(timeDTF));
            endTimeComboBox.getSelectionModel().select(LocalTime.of(8, 15).format(timeDTF));

        } catch (SQLException ex) {
            Logger.getLogger(AddAppointmentSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
