/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195.Model;

/**
 *
 * @author limpFish
 */
public class User {
    static int UserId;
    static String Username;
    static String Password;
    
    
    public User(int userId, String username, String password)
    {
        UserId = userId;
        Username = username;
        Password = password;
    }
    
    public User()
    {
        
    }
    
    public static void setUserId(int userId)
    {
        UserId = userId;
    }
    
    public static int getUserId()
    {
        return UserId;
    }
    
    public static void setUsername(String username) 
    {
        Username = username;
    }
    
    public static String getUserName()
    {
        return Username;
    }
    
    public static void setPassword(String password)
    {
        password = Password;
    }
    
    public String getPassword()
    {
        return Password;
    }
}
