/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195.View;

import c195.Model.Appointment;
import c195.Helpers.DatabaseUtils;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author limpFish
 */
public class CalendarSceneController implements Initializable {

    @FXML
    private TableView<Appointment> calendarTableView;
    @FXML
    private TableColumn<Appointment, String> calendarTableViewConsultantTableColumn;
    @FXML
    private TableColumn<Appointment, String> calendarTableViewAppointmentTypeTableColumn;
    @FXML
    private TableColumn<Appointment, String> calendarTableViewBeginTimeTableColumn;
    @FXML
    private TableColumn<Appointment, String> calendarTableViewEndTimeTableColumn;
    @FXML
    private TableColumn<Appointment, String> calendarTableViewClientTableColumn;
    @FXML
    private Button returnButton;
    @FXML
    private Button quitButton;
    @FXML
    private Button addButton;
    @FXML
    private Button modifyButton;
    @FXML
    private Button deleteButton;
    ObservableList<Appointment> apptList;
    private final DateTimeFormatter timeDTF = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
    private final ZoneId newzid = ZoneId.systemDefault();
    Utilities util = new Utilities();

    @FXML
    private void returnButtonPushed(ActionEvent event) throws IOException {
        util.switchScene(event, "MainScene.fxml");
    }

    @FXML
    private void quitButtonPushed(ActionEvent event) throws IOException {
        util.quitProgram();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        calendarTableViewConsultantTableColumn.setCellValueFactory(new PropertyValueFactory<Appointment, String>("user"));
        calendarTableViewAppointmentTypeTableColumn.setCellValueFactory(new PropertyValueFactory<Appointment, String>("type"));
        calendarTableViewBeginTimeTableColumn.setCellValueFactory(new PropertyValueFactory<Appointment, String>("start"));
        calendarTableViewEndTimeTableColumn.setCellValueFactory(new PropertyValueFactory<Appointment, String>("end"));
        calendarTableViewClientTableColumn.setCellValueFactory(new PropertyValueFactory<Appointment, String>("customer")); // TO DO --> must show customer name and not the object reference name
        apptList = FXCollections.observableArrayList();

        updateCalendarTableView();

    }

    private void updateCalendarTableView() {
        // TO DO --> insert times adjusted to universal time (UTC? / GMT?)

        try {

            PreparedStatement calendarSQL = DatabaseUtils.getDBConnection().prepareStatement("select appointment.appointmentId, customer.customerName, appointment.type, appointment.description, appointment.start, appointment.end,  user.userName, appointment.userId, appointment.customerId from appointment inner join customer on customer.customerId = appointment.customerId inner join user on user.userId = appointment.userId;");
            ResultSet calendarList = calendarSQL.executeQuery();
            apptList.clear();
            while (calendarList.next()) {

                String tAppointmentId = calendarList.getString("appointment.appointmentId");
                Timestamp tsStart = calendarList.getTimestamp("appointment.start");
                ZonedDateTime newzdtStart = tsStart.toLocalDateTime().atZone(ZoneId.of("UTC"));
                ZonedDateTime newLocalStart = newzdtStart.withZoneSameInstant(newzid);

                Timestamp tsEnd = calendarList.getTimestamp("appointment.end");
                ZonedDateTime newzdtEnd = tsEnd.toLocalDateTime().atZone(ZoneId.of("UTC"));
                ZonedDateTime newLocalEnd = newzdtEnd.withZoneSameInstant(newzid);

                String tType = calendarList.getString("appointment.type");

                String tCustomer = calendarList.getString("customer.customerName");

                String tUser = calendarList.getString("user.userName");

                String tDescription = calendarList.getString("appointment.description");

                apptList.add(new Appointment(tAppointmentId, tCustomer, tType, tDescription, newLocalStart.format(timeDTF), newLocalEnd.format(timeDTF), tUser));
                calendarTableView.getItems().setAll(apptList);
            }

        } catch (SQLException sqe) {
            sqe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void deleteButtonPushed() {
        Appointment appt = calendarTableView.getSelectionModel().getSelectedItem();

        try {
            PreparedStatement pst = DatabaseUtils.getDBConnection().prepareStatement("DELETE appointment.* FROM appointment WHERE appointment.appointmentId = ?");
            pst.setString(1, appt.getAppointmentId());
            pst.executeUpdate();
            updateCalendarTableView();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void addButtonPushed(ActionEvent event) throws IOException {
        util.switchScene(event, "AddAppointmentScene.fxml");
    }

    @FXML
    private void modifyButtonPushed(ActionEvent event) throws IOException, SQLException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("ModifyAppointmentScene.fxml"));
        Parent modifyAppointmentScreenParent = loader.load();
        ModifyAppointmentSceneController modifyAppointmentSceneController = loader.getController();

        // Send the data into ModifyAppointmentSceneController
        modifyAppointmentSceneController.initData(Integer.parseInt(calendarTableView.getSelectionModel().getSelectedItem().getAppointmentId()));

        Scene modifyAppointmentScreenScene = new Scene(modifyAppointmentScreenParent);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(modifyAppointmentScreenScene);
        window.show();
    }
}
