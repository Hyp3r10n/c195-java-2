/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195.Helpers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author limpFish
 */
public class DatabaseUtils {
    
    private static Connection connDB;
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB = "U05rm6";
    private static final String URL = "jdbc:mysql://52.206.157.109/" + DB;
    private static final String USER = "U05rm6";
    private static final String PASS = "53688588486";
    
    public static void init()
    {
        try 
        {
            Class.forName(DRIVER);
            connDB = DriverManager.getConnection(URL, USER, PASS);
            System.out.println("Connected to database: " + DB);
        }
        catch (SQLException e) 
        {
            System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
        }
        catch (ClassNotFoundException e)
        {
            System.out.println("ClassNotFoundException: " + e.getMessage());
        }
        
    }
    
    public static Connection getDBConnection()
    {
        return connDB;
    }
    
    public static void closeDBConnection()
    {
        try 
        {
            connDB.close();
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(DatabaseUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
