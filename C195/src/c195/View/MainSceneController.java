/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195.View;

import c195.Helpers.DatabaseUtils;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

/**
 * FXML Controller class
 *
 * @author limpFish
 */
public class MainSceneController implements Initializable {

    @FXML
    private Button customersButton;
    @FXML
    private Button calendarButton;
    @FXML
    private Button reportsButton;
    @FXML
    private Button logsButton;
    @FXML
    private Button logoutButton;
    @FXML
    private Button quitButton;
    Utilities util = new Utilities();

    @FXML
    private void customersButtonPushed(ActionEvent event) throws IOException {
        util.switchScene(event, "CustomersScene.fxml");
    }

    @FXML
    private void calendarButtonPushed(ActionEvent event) throws IOException {
        util.switchScene(event, "CalendarScene.fxml");
    }

    @FXML
    private void reportsButtonPushed(ActionEvent event) throws IOException {
        util.switchScene(event, "ReportsScene.fxml");
    }

    @FXML
    private void logsButtonPushed(ActionEvent event) throws IOException {
        util.switchScene(event, "LogsScene.fxml");
    }

    @FXML
    private void logoutButtonPushed(ActionEvent event) throws IOException {
        DatabaseUtils.closeDBConnection();
        System.out.println("Database connection closed successfully");
        util.switchScene(event, "LoginScene.fxml");
    }

    @FXML
    private void quitButtonPushed(ActionEvent event) throws IOException {
        util.quitProgram();
    }

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

}
