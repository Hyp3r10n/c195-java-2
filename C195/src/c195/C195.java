/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author limpFish
 */
public class C195 extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        String basePath = "C:/Users/limpFish/Documents/repositories/c195-java-2/C195/src/c195/View/";
        String fullPath = basePath + "LoginScene.fxml";
        String relativePath = "View/LoginScene.fxml";
        Parent root = FXMLLoader.load(getClass().getResource(relativePath));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
