/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195.View;

import c195.Model.User;
import c195.Model.LoggerPro;
import c195.Helpers.DatabaseUtils;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 *
 * @author limpFish
 */
public class LoginSceneController implements Initializable {

    @FXML
    private Button loginButton;
    @FXML
    private Button quitButton;
    @FXML
    private TextField usernameTextField;
    @FXML
    private TextField passwordPasswordField;
    @FXML
    private Label languageLabel;
    @FXML
    private ComboBox languageComboBox;
    @FXML
    private Label loginLabel;
    @FXML
    private Label userNameLabel;
    @FXML
    private Label passwordLabel;
    String errorMessage;
    public User user = new User();
    Utilities util = new Utilities();

    @FXML
    private void loginButtonPushed(ActionEvent event) throws SQLException, IOException {
        int dbUserId = 0;
        String dbUsername = null;
        String dbPassword = null;
        Date currentTime = Calendar.getInstance().getTime();

        String username = usernameTextField.getText();
        String password = passwordPasswordField.getText();

        try {

            LoggerPro logger = new LoggerPro();
            logger.log(username, currentTime);

            DatabaseUtils.init();
            PreparedStatement sql_login = DatabaseUtils.getDBConnection().prepareStatement("SELECT userId, userName, password FROM user WHERE userName = ? and password = ?");
            sql_login.setString(1, username);
            sql_login.setString(2, password);
            ResultSet result = sql_login.executeQuery();
            System.out.println(result.next());
            result.beforeFirst();
            if (result.next()) {
                dbUserId = result.getInt("userId");

                PreparedStatement appointmentTimeSelect = DatabaseUtils.getDBConnection().prepareStatement("select `start` from appointment where userId = ?;");
                appointmentTimeSelect.setInt(1, dbUserId);
                ResultSet appointmentTimes = appointmentTimeSelect.executeQuery();
                double fifteenMinutes = 900000;

                while (appointmentTimes.next()) {
//                    System.out.println(appointmentTimes.getDate("start").getTime() - currentTime.getTime() < fifteenMinutes);
                    if (appointmentTimes.getDate("start").getTime() - currentTime.getTime() < fifteenMinutes) {
                        appointmentAlert();
                    }
                }

                dbUsername = result.getString("userName");
                dbPassword = result.getString("password");

                if (username.equals(dbUsername)
                        & password.equals(dbPassword)) {
                    util.switchScene(event, "MainScene.fxml"); // FIXED by placing Utilities.java inside c195.View --> broken code: Location is not set. See also Utilities.java line 32
                }
            } else {
                System.out.println("Error triggered");
                Alert alert = new Alert(Alert.AlertType.ERROR,
                        errorMessage);
                alert.setHeaderText("Login Error");
                alert.showAndWait();
            }

        } catch (SQLException ex) {
            Logger.getLogger(LoginSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }

        User user = new User(dbUserId, username, password);
    }

    @FXML
    private void quitButtonPushed() throws IOException {
        util.quitProgram();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        passwordPasswordField = new PasswordField();
        languageComboBox.getItems().clear();
        languageComboBox.getItems().addAll("English", "Espanol", "Francais");
        languageComboBox.setOnAction((e) -> {
            String chosenLanguage = languageComboBox.getSelectionModel().getSelectedItem().toString();
            switch (chosenLanguage) {
                case "Espanol":
                    loginLabel.setText("Iniciar sesión");
                    userNameLabel.setText("Nombre de usuario");
                    passwordLabel.setText("Contraseña");
                    languageLabel.setText("Idioma");
                    loginButton.setText("Iniciar sesión");
                    quitButton.setText("Dejar");
                    errorMessage = "Nombre de usuario y contraseña no coinciden.";
                    break;
                case "Francais":
                    loginLabel.setText("S'identifier");
                    userNameLabel.setText("Nom d'utilisateur");
                    passwordLabel.setText("Mot de passe");
                    languageLabel.setText("La langue");
                    loginButton.setText("S'identifier");
                    quitButton.setText("Quitter");
                    errorMessage = "Nom d'utilisateur et mot de passe ne correspondent pas.";
                    break;
                default:
                    loginLabel.setText("Login");
                    userNameLabel.setText("Username");
                    passwordLabel.setText("Password");
                    languageLabel.setText("Language");
                    loginButton.setText("Login");
                    quitButton.setText("Quit");
                    errorMessage = "Username and password do not match.";
                    break;
            }
        });
    }

    private void appointmentAlert() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION,
                "Time for appt",
                ButtonType.OK);

        alert.setHeaderText("Quit Confirmation");
        alert.showAndWait()
                .filter(response -> response == ButtonType.YES) // Lambda chosen over other options for concise solution
                .ifPresent(response -> {
                    Platform.exit();
                    System.exit(0);
                });
    }
}
