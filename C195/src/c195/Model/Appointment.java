/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195.Model;

/**
 *
 * @author limpFish
 */
public class Appointment {

    private String appointmentId;
    private String customer;
    private String type;
    private String description;
    private String start;
    private String end;
    private String user;
    private String title;

    public Appointment(String appointmentId, String customer, String type, String description, String start, String end, String user) {
        this.appointmentId = appointmentId;
        this.customer = customer;
        this.type = type;
        this.description = description;
        this.start = start;
        this.end = end;
        this.user = user;
    }
    
        public Appointment(String appointmentId, String customer, String type, String description, String start, String end, String user, String title) {
        this.appointmentId = appointmentId;
        this.customer = customer;
        this.type = type;
        this.description = description;
        this.start = start;
        this.end = end;
        this.user = user;
        this.title = title;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    public String getAppointmentId() {
        return this.appointmentId;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getCustomer() {
        return this.customer;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getStart() {
        return this.start;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getEnd() {
        return this.end;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUser() {
        return this.user;
    }

}
