/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195.View;

import c195.Model.User;
import c195.Model.Customer;
import c195.Helpers.DatabaseUtils;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author limpFish
 */
public class ModifyCustomerSceneController implements Initializable {

    @FXML
    private TextField nameTextField;
    @FXML
    private TextField phoneTextField;
    @FXML
    private TextField addressTextField;
    @FXML
    private TextField address2TextField;
    @FXML
    private TextField postalCodeTextField;
    @FXML
    private Label countryLabel;
    @FXML
    private Button saveButton;
    @FXML
    private Button cancelButton;
    @FXML
    private ComboBox cityComboBox;
    private int customerId;
    private int addressId;
    Utilities util = new Utilities();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        cityComboBox.getItems().addAll("New York", "Phoenix", "London");

        cityComboBox.setOnAction((e) -> {
            String country = cityComboBox.getSelectionModel().getSelectedItem().toString();
            switch (country) {
                case "New York":
                    country = "United States";
                    break;
                case "Phoenix":
                    country = "United States";
                    break;
                case "London":
                    country = "United Kingdom";
                    break;
                default:
                    country = "Unknown";
                    break;
            }

            countryLabel.setText(country);

        });

    }

    @FXML
    public void initData(Customer cust) {
        Customer customer = cust;
        nameTextField.setText(customer.getName());
        phoneTextField.setText(customer.getPhone());
        addressTextField.setText(customer.getAddress());
        address2TextField.setText(customer.getAddress2());
        postalCodeTextField.setText(customer.getPostalCode());
        cityComboBox.getSelectionModel().select(customer.getCity());
        countryLabel.setText(customer.getCountry());

        customerId = customer.getCustomerId();

    }

    @FXML
    private void cancelButtonPushed(ActionEvent event) throws IOException {
        util.switchScene(event, "CustomersScene.fxml");
    }

    @FXML
    private void saveButtonPushed(ActionEvent event) throws SQLException, IOException {

        //1. grab addressId to update
        PreparedStatement addressIdSQL = DatabaseUtils.getDBConnection().prepareStatement("select addressId from customer where customerId = ?;");
        addressIdSQL.setInt(1, customerId);
        ResultSet result = addressIdSQL.executeQuery();
        result.next();
        addressId = result.getInt("addressId");

        //2. grab cityId to update based on cityComboBox current selection
        String cityName = cityComboBox.getValue().toString();
        PreparedStatement cityIdSQL = DatabaseUtils.getDBConnection().prepareStatement("select cityId from city where city = ?;");
        cityIdSQL.setString(1, cityName);
        ResultSet cityResult = cityIdSQL.executeQuery();
        cityResult.next();
        int cityId = cityResult.getInt("cityId");

        //3. reSET all address fields from the view
        PreparedStatement addressUpdateSQL = DatabaseUtils.getDBConnection().prepareStatement("update address set address = ?, address2 = ?, cityId = ?, postalCode = ?, phone = ?, lastUpdate=now(), lastUpdateBy = ? where addressId = ?;");
        addressUpdateSQL.setString(1, addressTextField.getText());
        addressUpdateSQL.setString(2, address2TextField.getText());
        addressUpdateSQL.setInt(3, cityId);
        addressUpdateSQL.setString(4, postalCodeTextField.getText());
        addressUpdateSQL.setString(5, phoneTextField.getText());
        addressUpdateSQL.setString(6, User.getUserName());
        addressUpdateSQL.setInt(7, addressId);
        addressUpdateSQL.executeUpdate();

        //4. reSET all customer fields from the view
        PreparedStatement customerUpdateSQL = DatabaseUtils.getDBConnection().prepareStatement("update customer set customerName = ?, addressId = ?, lastUpdate = now(), lastUpdateBy = ? where customerId = ?;");
        customerUpdateSQL.setString(1, nameTextField.getText());
        customerUpdateSQL.setInt(2, addressId);
        customerUpdateSQL.setString(3, User.getUserName());
        customerUpdateSQL.setInt(4, customerId);
        customerUpdateSQL.executeUpdate();

        util.switchScene(event, "CustomersScene.fxml");
    }

}
