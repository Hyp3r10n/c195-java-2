/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195.Model;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 *
 * @author limpFish
 */
public class LoggerPro {

    private File f;

    public LoggerPro() {

    }

    public void log(String userName, Date timestamp) {
        createTextFile();
        FileWriter writer = null;
        try {
            writer = new FileWriter(f, true);
            writer.append(userName + " " + timestamp);
            writer.append("\n");
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(LoggerPro.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void createTextFile() {
        f = new File("C:/Users/Hyper10n/Documents/repositories/c195-java-2/C195Log.txt");
        if (!f.exists()) {
            try {
                f.createNewFile();
                System.out.println("File created");
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(LoggerPro.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public Stream<String> readLogs() {
        Stream<String> stream = null;
        f = new File("C:/Users/limpFish/Desktop/hi.txt");
        if (f.exists()) {

            try {
                stream = Files.lines(Paths.get(f.getAbsolutePath()));

//            stream.forEach(System.out::println);  // This works
            } catch (IOException ex) {
                Logger.getLogger(LoggerPro.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return stream;
    }

}
